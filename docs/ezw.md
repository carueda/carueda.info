
![](/img/einstein_0.jpg)

This is my Java implementation of the EZW image codec according to Shapiro's paper:

- ["Embedded Image Coding Using Zerotrees of Wavelet Coefficients."](http://dx.doi.org/10.1109/78.258085)
IEEE Trans. on signal proc. Vol. 41, No 12, December, 1993.

Git repository: [https://github.com/carueda/ezw](https://github.com/carueda/ezw).

To compile the package, just type:

    ant

which assumes you have the Apache Ant tool.

To run a complete encode/decode test:

    cd work/
    make image=al.pgm filter=haar

this will encode an image using the program is.ezw.SAQ and then the encoded bit
stream will be decoded and displayed progressively using the program
is.ezw.ProgressiveViewer. If you don't have a unix-like environment, just have a
look at the Makefile to see how you can call the java programs directly.

![](/img/EZW.gif)


Included images:

- `al.pgm    ` 256×256
- `lena.jpg  ` 512×512
- `ingrid.raw` 256×256
- `uam.jpg   ` 256×256

Any image that java can read can be used; the only requirement is that it be
square with side length a power of 2. If the image is in color, its green
component is used for all processing.

These filters are implemented:

- haar
- daub4
- daub12

See the Makefile for other parameters.

Note: this was mainly a personal learning exercise. I put some effort for a
clear design but couldn't find the time to properly comment the source code.
Anyway, hope you find the code and the demo program useful.

