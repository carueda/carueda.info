## Projects

You can also look at
<a href="https://mbari.org/carlos-rueda">
mbari.org/carlos-rueda
</a>.


#### StarSpan - A tool for fast data extraction from remotely-sensed imagery.
[http://starspan.projects.atlas.ca.gov](starspan).

Quadtree based algorithms for fast vector-raster intersection and extraction of pixels.


#### SAMS - Spectral Analysis and Management System.
<https://github.com/carueda/sams/wiki>.

Application to
manage field spectra databases and perform some typical pre-processing analyses.
Features include: import/export options in various formats, signature groupings,
metadata management, common operations on single or multiple signatures, richer
set of plotting capabilities, and a simple data structure for easy integration
with other applications.


#### GOES - Reference Evapotranspiration calculation for California.
<http://goes.casil.ucdavis.edu/>.

Interpolation of ground
measured variables (air temperature, vapor pressure, etc.), solar position
calculations, and automated download of data from CIMIS stations.

#### RTM - Radiative Transfer Model Repository.
<http://rtm.projects.atlas.ca.gov/>.

A repository
of programs and programming libraries implementing Radiative Transfer Models for
Remote Sensing research and applications.

#### [ECOZ2](https://github.com/ecoz2/)

Linear Predictive Coding Vector Quantization and Hidden Markov Modeling for speech recognition.

#### [EZW](/ezw)

A Java implementation of the Embedded Zerotree
Wavelet image codec (Shapiro, 1993) for image compression.

#### [Loro](https://sourceforge.net/projects/loro/)

A Programming Language and Integrated Development Environment for Beginners.

#### red_edge_gauss, spectral_index_calculation.

Programs to compute the [red edge](https://en.wikipedia.org/wiki/Red_edge)
by using an inverted-Gaussian based method, and to compute
user-defined spectral indices on remote sensing images.
