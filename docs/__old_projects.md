## Projects

#### Targeted Sampling by Autonomous Underwater Vehicles

Wave Glider autonomous front detection and tracking.

An exercise involving monitoring of seawater near Hawaiian lava flow in 2018
is described [here](https://www.mbari.org/lava-flow-front-tracking/).

![](/img/tft-hawaii.png ':size=400')


#### [Long-Range Autonomous Underwater Vehicles](https://www.mbari.org/at-sea/vehicles/autonomous-underwater-vehicles/long-range-auv-tethys/)

[TethysDash](https://docs.mbari.org/tethysdash/) shore side server and
web applications for visualization and vehicle command and control.
[TethysL](https://docs.mbari.org/tethysl/) domain specific language for mission script authoring.

#### Cross-Domain Observational Metadata for Environmental Sensing (X-DOMES) - <https://xdomes.org>

An NSF/EarthCube project tasked to develop prototypes for the application of
standards-based description of environmental sensor metadata, including provenance,
that is paramount to automated data discovery, access, archival, processing,
as well as quality control and assessment.

#### [Environmental Sample Processor (ESP)](https://www.mbari.org/technology/emerging-current-tools/instruments/environmental-sample-processor-esp/)

LRAUV integration. Servo microcontroller simulation.

#### [Oceanographic Decision Support System](https://www.mbari.org/science/upper-ocean-systems/canon/)

Visualization and planning components in the ODSS decision support system to
help understand the complex physical and biological ocean processes that lead
to ephemeral features in the ocean, such as harmful algal blooms and microbial
activities that drive cycling of life-sustaining elements.

Components to support multi-vehicle, autonomous, goal-oriented
experiments while the assets adapt to changing conditions.

#### [OOI-CI: Ocean Observatories Initiative Cyberinfrastructure](https://oceanobservatories.org/)

Platform Agent framework. Integration of the
[PUCK](https://www.mbari.org/mbari-developed-puck-protocol-adopted-by-international-standards-group/) and
[SIAM](https://www.mbari.org/products/research-software/siam-documentation/) MBARI technologies.


#### Marine Metadata Interoperability Project - <br>
 <https://mmisw.org> - <https://marinemetadata.org>


#### [Observatory Middleware Framework](https://sourceforge.net/projects/omfesb/)

Research of alternative cyberinfrastructure approaches that support
multi-domain research, integrate existing instrument networks with a common
instrument proxy, and support a set of security capabilities critical for
community-owned observatories.


####  [COMET - The COast-to-Mountain Environmental Transect Project](http://www.nsf.gov/awardsearch/showAward?AWD_ID=0619139)
- <http://comet.ucdavis.edu/>

Web applications for real-time visualization of sensor data streams based on open software components and standards.

#### [GeoStreams - Adaptive Query Processing Architecture for Streaming Geospatial Image Data](http://www.nsf.gov/awardsearch/showAward?AWD_ID=0326517).

Real-time visualization tool for streaming satellite images based on a formal
image stream model.


#### StarSpan - A tool for fast data extraction from remotely-sensed imagery.
[http://starspan.projects.atlas.ca.gov](starspan).

Quadtree based algorithms for fast vector-raster intersection and extraction of pixels.


#### SAMS - Spectral Analysis and Management System.
<https://github.com/carueda/sams/wiki>.

Application to
manage field spectra databases and perform some typical pre-processing analyses.
Features include: import/export options in various formats, signature groupings,
metadata management, common operations on single or multiple signatures, richer
set of plotting capabilities, and a simple data structure for easy integration
with other applications.


#### GOES - Reference Evapotranspiration calculation for California.
<http://goes.casil.ucdavis.edu/>.

Interpolation of ground
measured variables (air temperature, vapor pressure, etc.), solar position
calculations, and automated download of data from CIMIS stations.

#### RTM - Radiative Transfer Model Repository.
<http://rtm.projects.atlas.ca.gov/>.

A repository
of programs and programming libraries implementing Radiative Transfer Models for
Remote Sensing research and applications.

#### [ECOZ2](https://github.com/ecoz2/)

Linear Predictive Coding Vector Quantization and Hidden Markov Modeling for speech recognition.

#### [EZW](/ezw)

A Java implementation of the Embedded Zerotree
Wavelet image codec (Shapiro, 1993) for image compression.

#### [Loro](https://sourceforge.net/projects/loro/)

A Programming Language and Integrated Development Environment for Beginners.

#### red_edge_gauss, spectral_index_calculation.

Programs to compute the [red edge](https://en.wikipedia.org/wiki/Red_edge)
by using an inverted-Gaussian based method, and to compute
user-defined spectral indices on remote sensing images.
