<div style="color: #9a9a9a">
(For my professional profile, you can visit
<a href="https://mbari.org/carlos-rueda">
mbari.org/carlos-rueda
</a>)
</div>

_Any opinions in this site are my own._

---

_The true mystery of the world is the visible, not the invisible._ —Oscar Wilde

---

#### some of my personal interests

- Music!
  - Johann Sebastian Bach
  - Classical Guitar ([Agustin Barrios](https://www.cybozone.com/fg/jeong.html),
    [Gentil Montaña](https://es.wikipedia.org/wiki/Julio_Albarrac%C3%ADn_Monta%C3%B1a))
  - Folk music from all over the world
- [Charter for Compassion](https://charterforcompassion.org/)
- [Gabriel García Márquez](https://www.nobelprize.org/prizes/literature/1982/marquez/lecture/)
  (The Nobel Prize in Literature 1982)
- Comparative religion - trying to understand why some people become polarized.

#### some links

- <http://www.earth-policy.org/>. “No economy, however technologically
  advanced, can survive the collapse of its environmental support systems.”
  Lester R. Brown, Plan B 2.0: Rescuing a Planet Under Stress and a Civilization in Trouble (NY: W.W. Norton & Co., 2006).

- <https://www.worldcommunitygrid.org/>. “Technology solving problems: Simply
  donate the time your computer is turned on, but would normally lie idle, for projects that benefit humanity.”
