
#### StarSpan - Fast algorithms for raster pixel extraction from geometry features

StarSpan was designed to bridge the raster and vector worlds of spatial analysis
using fast algorithms for pixel level extraction from geometry features (points,
lines, polygons). StarSpan generates databases of extracted pixel values
(from one or a set of raster images), fused with the database attributes
from the vector files. This allows a user to do statistical analysis of the
pixel vs. attribute data in many existing packages and can greatly speed up
classification training and testing.

The tool has been used in multiple projects at several research institutions worldwide,
see [citations according to Google Scholar](https://scholar.google.com/scholar?oi=bibs&hl=en&cites=7834898888893508076).

Original code can be found on Github at [carueda/starspan](https://github.com/carueda/starspan),
but several [more recent forks](https://github.com/carueda/starspan/wiki) have been created by the community, including:

- <https://github.com/theotherfirm/starspan>
- <https://github.com/Ecotrust/starspan>
- <https://github.com/tokumine/Starspan>


This project was possible thanks to [California State Department of Boating and Waterways](http://www.dbw.ca.gov/).
Originally located at http://starspan.projects.atlas.ca.gov, StarSpan was developed at
[University of California Davis Center for Spatial Technologies and Remote Sensing (CSTARS)](http://cstars.metro.ucdavis.edu/).
