
#### links

- <http://www.earth-policy.org/>. “No economy, however technologically
advanced, can survive the collapse of its environmental support systems.”
 Lester R. Brown, Plan B 2.0: Rescuing a Planet Under Stress and a Civilization in Trouble (NY: W.W. Norton & Co., 2006).

- <http://www.worldcommunitygrid.org/>. “Technology solving problems: Simply
donate the time your computer is turned on, but would normally lie idle, for projects that benefit humanity.”



#### personal interests

- Classical Guitar ([Agustin Barrios](http://www.cybozone.com/fg/jeong.html),
[Gentil Montaña](http://www.caronimusic.com/pagesAnglais/Montana.php?idsession=40001107370896&pagec=0))
- [Johann Sebastian Bach](http://www.jsbach.org/)
- [Science at the Crossroads](http://www.dalailama.com/messages/buddhism/science-at-the-crossroads), by Tenzin Gyatso
- [Charter for Compassion](http://charterforcompassion.org/)
- [Gabriel García Márquez](http://www.nobel.se/literature/laureates/1982/) (The Nobel Prize in Literature 1982)
- Comparative religion


#### misc

- [Loro](http://loro.sourceforge.net/z/): My own programming language
- <http://uepage.org>. Promoting Colombian culture in Davis and Northern California
- [Colombian coffee](http://www.juanvaldez.com/). Considered one of the most delightful ones in the world
from high mountain cultivation, careful hand-picking and innovative processing techniques.


#### `/dev/null`

![](https://www.roe.ac.uk/~pnb/images/Black-hole.jpg)
