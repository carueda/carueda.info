Some wonderful nonagenarians:

- Thich Nhat Hạnh (1926–2022) – "There is no way to happiness. Happiness is the way."
- Desmond Tutu (1931–2021)
- Noam Chomsky (1928)
- David Attenborough (1926)
